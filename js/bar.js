async function cargarBebidas() {
        const tipo = document.querySelector('input[name="tipo"]:checked').value;
    try {
        const response = await fetch(`https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=${tipo}`);
        const data = await response.json();
        const cocteles = document.getElementById('cocteles');
        cocteles.innerHTML = '';
        data.drinks.forEach(drink => {
            const div = document.createElement('div');
            const imagen = document.createElement('img');
            const nombre = document.createElement('p');

            imagen.src = drink.strDrinkThumb;
            imagen.alt = drink.strDrink;
            nombre.textContent = drink.strDrink;
            div.id="bebidas"

            div.appendChild(imagen);
            div.appendChild(nombre);

            cocteles.appendChild(div);
        });

        document.getElementById('contar').textContent = `Total de bebidas: ${data.drinks.length}`;
    } catch (error) {
        console.error('Error al cargar las bebidas:', error);
    }
}

document.addEventListener("DOMContentLoaded", function () {
    document.getElementById("btnBuscar").addEventListener("click", function () {
        cargarBebidas();
    });
});

document.getElementById("btnLimpiar").addEventListener('click', function(){
    document.getElementById("cocteles").innerHTML=' ';
    document.getElementById("contar").innerHTML=" ";
})
